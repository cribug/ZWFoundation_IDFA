Pod::Spec.new do |s|
s.name                = "ZWFoundation"
s.version             = "3.0.5"
s.summary             = 'mob.com iOS 端 SDK 的公共库'
s.license             = 'Copyright © 2012-2016 mob.com'
s.author              = { "Cribug" => "iwilliamchang@outlook.com" }
s.homepage            = 'http://www.mob.com'
s.source              = { :git => "https://git.oschina.net/cribug/ZWFoundation_IDFA.git", :tag => s.version.to_s }
s.platform            = :ios, '7.0'
s.frameworks          = "JavaScriptCore"
s.libraries           = "icucore", "z", "stdc++"
s.vendored_frameworks = 'MOBFoundation.framework','MOBFoundationEx.framework'
end
